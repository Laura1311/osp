/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

//BEGIN_INCLUDE(all)
#include <initializer_list>
#include <memory>
#include <jni.h>
#include <errno.h>
#include <cassert>

#include <EGL/egl.h>
#include <GLES/gl.h>

#include <android/sensor.h>
#include <android/log.h>
#include <android_native_app_glue.h>



//END_INCLUDE(all)
void ANativeActivity_onCreate(ANativeActivity* activity,
        void* savedState, size_t savedStateSize);


void draw_something(struct android_app* app) {


        	ANativeWindow* lWindow = app->window;
            //set the format of the window, 0 for default width and height
            ANativeWindow_setBuffersGeometry(lWindow, 0, 0,
            	WINDOW_FORMAT_RGBA_8888);

            	ANativeWindow_Buffer lWindowBuffer;
                //ANativeWindow* lWindow = app->window;
                //acquire the window buffer
                if (ANativeWindow_lock(lWindow, &lWindowBuffer,
                	NULL) < 0) {
                	return;
                }
                //clear the buffer
                memset(lWindowBuffer.bits, 0, lWindowBuffer.
                	stride*lWindowBuffer.height*sizeof(uint32_t));

                	int sqh = 400, sqw = 400;
                    int wst = lWindowBuffer.stride/2 - sqw/2;
                    int wed = wst + sqw;
                    int hst = lWindowBuffer.height/2 - sqh/2;
                    int hed = hst + sqh;
                    for (int i = hst; i < hed; ++i) {
                    	for (int j = wst; j < wed; ++j) {
                    		((char*)(lWindowBuffer.bits))
                    	[(i*lWindowBuffer.stride + j)*sizeof(uint32_t)] = (char)40;
                    		((char*)(lWindowBuffer.bits))
                    	[(i*lWindowBuffer.stride + j)*sizeof(uint32_t) + 1] = (char)191;
                    		((char*)(lWindowBuffer.bits))
                    	[(i*lWindowBuffer.stride + j)*sizeof(uint32_t) + 2] = (char)140;
                    		((char*)(lWindowBuffer.bits))
                    	[(i*lWindowBuffer.stride + j)*sizeof(uint32_t) + 3] = (char)255;
                    	}
                    }

                    ANativeWindow_unlockAndPost(lWindow);


        }

		void handle_activity_lifecycle_events(struct android_app* app,
        		int32_t cmd) {
        	__android_log_print(ANDROID_LOG_INFO, "Native", "%d: dummy data %p", cmd, ((int*)(app->userData)));

        	switch (cmd) {

        		case APP_CMD_INIT_WINDOW:
        			draw_something(app);
        			break;

        		case APP_CMD_WINDOW_REDRAW_NEEDED:
        			draw_something(app);

        	}
        }

        int32_t handle_activity_input_events(struct android_app* app,
        		AInputEvent *event) {
        	if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION) {
        		__android_log_print(ANDROID_LOG_INFO, "Native", "%d %d: dummy data %p",
        					AInputEvent_getType(event), AMotionEvent_getAction(event), ((int*)(app->userData)));
        		return 1;
        	}
        	return 0;
        }

        void android_main(struct android_app* app) {
        	app_dummy();
        	// Make sure glue isn't stripped.
        	app->userData = NULL;
        	app->onAppCmd = handle_activity_lifecycle_events;
        	app->onInputEvent = handle_activity_input_events;

        	while (1) {
        		int ident, events;
        		struct android_poll_source* source;
        		if ((ident=ALooper_pollAll(-1, NULL, &events, (void**)&source)) >=
        				0) {
        			source->process(app, source);
        		}
        	}

        }





