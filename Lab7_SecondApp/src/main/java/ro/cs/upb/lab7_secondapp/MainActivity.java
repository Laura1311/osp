package ro.cs.upb.lab7_secondapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();

// The action string represents the intent filter name defined in the manifest file of the first application
//                i.setPackage("ro.cs.upb.lab7");
                i.setAction("ro.cs.upb.lab7.startSecondActivity");

                startActivity(i);
            }
        });

    }
}
