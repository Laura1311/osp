package ro.cs.upb.lab8;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {

    private TextView textView;
    SSLContext sslCtx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        textView = (TextView) findViewById(R.id.text_view);
        final EditText input = (EditText) findViewById(R.id.input_value);
        Button button = (Button) findViewById(R.id.send_button);

//        adb shell input text "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=48.8589507,2.2775174\&radius=500\&type=museum\&key=AIzaSyDm1Nes4hK-BGDOCKtacS11m8gCLZ2qdl0"


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connectivityManager = (ConnectivityManager) MainActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

                if (isConnected) {
                    new Task1().execute(input.getText().toString());
                }

            }
        });

        //task 3

        ListView listView = (ListView) findViewById(R.id.listView);
        List<String> trustAnchorList = new ArrayList<>();

        TrustManagerFactory tmf;
        try {
            tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init((KeyStore) null);
            X509TrustManager xtm = (X509TrustManager) tmf.getTrustManagers()[0];


            //task 4

            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream in = getResources().openRawResource(R.raw.mycertfile);

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            java.security.cert.Certificate certificate = cf.generateCertificate(in);

            keyStore.load(null, null);
            keyStore.setCertificateEntry("certificate", certificate);

            tmf.init(keyStore);

            sslCtx = SSLContext.getInstance("TLS");
            sslCtx.init(null, tmf.getTrustManagers(), null);

            for (X509Certificate cert : xtm.getAcceptedIssuers()) {
                String certStr = "S:" + cert.getSubjectDN().getName() + "\nI:"
                        + cert.getIssuerDN().getName();

                trustAnchorList.add(certStr);
                Log.d("MainActivity", certStr);
            }
        } catch (NoSuchAlgorithmException | KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.list_item, trustAnchorList);
        listView.setAdapter(arrayAdapter);



    }


    private class Task1 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            InputStream is = null;
            try {
                url = new URL(params[0]);

                //task 1
                //HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                //task 2
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();


                //task 4
                connection.setSSLSocketFactory(sslCtx.getSocketFactory());

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                return stringBuilder.toString();

            } catch (MalformedURLException e) {
                Log.d("MainActivity", "Cannot connect!");
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            textView.setText(s);
            super.onPostExecute(s);
        }
    }

}
