package ro.cs.upb.lab3;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    EditText enteredValue;
    TextView textView;
    IExtService iExtService;
    double output = 0;
    boolean mBound = false;

    ServiceConnection serviceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        enteredValue = (android.widget.EditText) findViewById(R.id.enter_value);
        Button button = (Button) findViewById(R.id.computeButton);
        textView = (TextView) findViewById(R.id.output_text);

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                iExtService = IExtService.Stub.asInterface(service);
                mBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
            }
        };

        final Intent i = new Intent();
        i.setPackage("ro.cs.upb.lab3");
        i.setAction("ro.cs.upb.lab3.IExtService.aidl");
        bindService(i, serviceConnection, Context.BIND_AUTO_CREATE);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enteredValue.getText() == null || enteredValue.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "INVALID INPUT", Toast.LENGTH_LONG).show();
                    return;
                }
                if (iExtService != null){
                    try {
                        output = iExtService.computePi(Integer.valueOf(enteredValue.getText().toString()));
                    } catch (RemoteException e) {
                        Log.d("COMPUTE PI ERROR:", e.getMessage());
                    }
                }

                textView.setText("Output PI: " + output);

            }
        });



    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(serviceConnection);
            mBound = false;
        }
    }
}
