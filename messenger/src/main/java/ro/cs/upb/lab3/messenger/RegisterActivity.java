package ro.cs.upb.lab3.messenger;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.*;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;


public class RegisterActivity extends Activity {

    final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle bundle = msg.getData();

            try {
                String resp = "{" + bundle.getString("resp");
                JSONObject response = new JSONObject(resp);

                //check for errors
                if (response.getInt("response") != 0) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(RegisterActivity.this);
                    dialog.setTitle("Cannot create user");
                    dialog.setMessage(response.getString("message"));

                    AlertDialog alertDialog = dialog.create();
                    alertDialog.show();
                } else {
                    //no need to redirect to login because server logs in every user that is registered with success
//                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
//                    startActivity(intent);
                }
            } catch (JSONException e) {
                Log.d("RegisterActivity", e.getMessage());
            }

        }
    };

    ConnectionService connectionService;
    ServiceConnection serviceConnection;
    boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText username = (EditText) findViewById(R.id.register_username);
        final EditText password = (EditText) findViewById(R.id.register_password);
        final EditText retypedPassword = (EditText) findViewById(R.id.register_retype_password);
        Button createUserButton = (Button) findViewById(R.id.create_user);

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                ConnectionService.MyBinder myBinder = (ConnectionService.MyBinder) service;
                connectionService = myBinder.getService();
                mBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
            }
        };

        Intent i = new Intent(this, ConnectionService.class);
        bindService(i, serviceConnection, Context.BIND_AUTO_CREATE);

        createUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (password.getText() == null || retypedPassword.getText() == null || password.getText().toString().equals("") || retypedPassword.getText().toString().equals("")) {
                    Toast.makeText(RegisterActivity.this, "INVALID INPUT", Toast.LENGTH_LONG).show();
                    return;
                }

                if (!password.getText().toString().equals(retypedPassword.getText().toString())) {
                    Toast.makeText(RegisterActivity.this, "PASSWORD MUST BE THE SAME", Toast.LENGTH_LONG).show();
                    return;
                }

                //create user on the server side
                try {
                    RegisterObject registerObject = new RegisterObject(username.getText().toString(),
                            password.getText().toString(), true);

//                    Intent intentToService = new Intent(RegisterActivity.this, ConnectionService.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putParcelable(Constants.REGISTER_OBJECT, registerObject);
//                    intentToService.putExtras(bundle);
//                    startService(intentToService);


                    if (connectionService != null) {
                        connectionService.registerUser(registerObject, mHandler);
                    }
                } catch (JSONException e) {
                    Log.d("RegisterActivity: ", "COULD NOT CREATE REGISTER OBJECT");
                }




            }
        });
    }



    private class SendfeedbackJob extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String[] params) {
            // do above Server call here
            return "some message";
        }

        @Override
        protected void onPostExecute(String message) {
            //process message
        }
    }

}
