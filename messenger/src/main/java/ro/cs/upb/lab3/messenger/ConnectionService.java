package ro.cs.upb.lab3.messenger;

import android.app.Service;
import android.content.Intent;
import android.os.*;
import android.util.Log;
import org.json.JSONException;

import java.io.*;
import java.net.Socket;

/**
 * @version $Id$
 */
public class ConnectionService extends Service {
    private final MyBinder myBinder = new MyBinder();
    private Handler callback;


    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Bundle bundle = intent.getExtras();
//
//        RegisterObject registerObject = bundle.getParcelable(Constants.REGISTER_OBJECT);
//        new ConnectionThread().execute(registerObject);

        return START_STICKY;
    }

    public void registerUser(RegisterObject registerObject, Handler mHandler) {
        this.callback = mHandler;
        new ConnectionThread().execute(registerObject);
    }


    private class ConnectionThread extends AsyncTask<RegisterObject, Void, String> {

        @Override
        protected String doInBackground(RegisterObject... params) {
            Socket socket;
            try {
                //Android emulator runs in a Virtual Machine so the emulator will have its own loopback address
                socket = new Socket("10.0.2.2", 4000);

                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream());
                PrintWriter printWriter = new PrintWriter(bufferedOutputStream, true);
                printWriter.println(params[0].getRegisterObject().toString() + '\0');
                printWriter.flush();

                InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
                if (inputStreamReader.ready()) {
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    //cannot use readLine because server isn't sending \n char
                    int length = bufferedReader.read();
                    char[] output = new char[length];
                    bufferedReader.read(output, 0, length);
                    socket.close();

                    String stringOutput = String.valueOf(output, 0, length);
                    return stringOutput.substring(0,stringOutput.lastIndexOf("}")+1);
                }
                socket.close();
            } catch (IOException e) {
                Log.d("ConnectionService: ", "SOCKET CREATION ERROR");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.d("ConnectionService: ", "onPostExecute");
            super.onPostExecute(response);

            if (response != null) {
                Log.d("ConnectionService: ", response);

                Message msg = callback.obtainMessage();
                Bundle args = new Bundle();
                args.putString("resp", response);
                msg.setData(args);
                msg.sendToTarget();
            }
        }
    }

    public class MyBinder extends Binder {
        ConnectionService getService() {
            return ConnectionService.this;
        }
    }

}
