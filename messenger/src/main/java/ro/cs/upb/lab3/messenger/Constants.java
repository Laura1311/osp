package ro.cs.upb.lab3.messenger;

/**
 * @version $Id$
 */
public class Constants {
    public static final String RECEIVE_ACTION = "RECEIVE_ACTION";
    public static final String INPUT = "INPUT";

    public static final String USERNAME = "user";
    public static final String PASSWORD = "pass";
    public static final String CREATE = "create";


    public static final String REGISTER_OBJECT = "REGISTER_OBJECT";
}
