package ro.cs.upb.lab3.messenger;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @version $Id$
 */
public class ResponseObject extends JSONObject implements Parcelable {

    private String code;
    private String message;

    public ResponseObject(String code, String message) throws JSONException {
        this.code = code;
        this.message = message;
    }

    public ResponseObject(Parcel in) {
        this.code = in.readString();
        this.message = in.readString();
    }

    public JSONObject getRegisterObject() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put(Constants.USERNAME, this.code);
        jsonObject.put(Constants.PASSWORD, this.message);

        return jsonObject;
    }

//    public static RegisterObject createRegisterObject(String username, String password, boolean create) throws JSONException {
//        RegisterObject registerObject = new RegisterObject();
//        registerObject.put(Constants.USERNAME, username);
//        registerObject.put(Constants.PASSWORD, password);
//        if (create) {
//            registerObject.put(Constants.CREATE, "true");
//        }
//
//        return  registerObject;
//    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeString(this.message);
    }
    public static final Creator CREATOR = new Creator() {
        public ResponseObject createFromParcel(Parcel in) {
            return new ResponseObject(in);
        }

        public ResponseObject[] newArray(int size) {
            return new ResponseObject[size];
        }
    };

}
