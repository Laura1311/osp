package com.example.user.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {


    static final int TRIES = 5;
    static Random r = new Random();
    static int prevInt = r.nextInt();
    static TextView correct = null;
    int field;
    boolean exception;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    void setText() {
        TextView text = (TextView) findViewById(R.id.textView1);
        if (text != null)
            text.setText("OK!");
    }

    void setText(String str) {
        TextView text = (TextView) findViewById(R.id.textView2);
        if (text != null)
            text.setText(str);
    }

    String str3;

    void setText(String str, int i) {
        TextView text = (TextView) findViewById(R.id.textView3);
        if (text == null)
            return;
        if (str3.compareTo(str) == 0 && i == 2) {
            text.setText("OK-TASK3!");
        } else {
            text.setText("Wrong.");
        }
    }

    String getInput() {
        EditText ed = (EditText) findViewById(R.id.editText3);
        if (ed == null)
            return null;
        str3 = ed.getText().toString();
        return str3;
    }

    static int getInt() {
        prevInt = r.nextInt();
        return prevInt;
    }

    static void checkInt(int r) throws Exception {
        if (prevInt != r) {
            if (correct != null)
                correct.setText("Wrong.");
        } else {
            if (correct != null)
                correct.setText("OK checkInt -TASK 4!");
        }
    }

    native void task1();
    native void task2();
    native void task3(int i);
    native void task4();
    native int task5();
    native void task6(int i);
    native void task7() throws Exception;
    native boolean task8();

    void generateException() throws Exception {
        if (exception) {
            throw new Exception();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b1 = (Button) findViewById(R.id.button1);
        b1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                task1();
            }
        });

        Button b2 = (Button) findViewById(R.id.button2);
        b2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                task2();
            }
        });

        Button b3 = (Button) findViewById(R.id.button3);
        b3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                task3(2);
            }
        });

        Button b4 = (Button) findViewById(R.id.button4);
        b4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                task4();
            }
        });

        Button b5 = (Button) findViewById(R.id.button5);
        b5.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TextView text = (TextView) findViewById(R.id.textView5);
                boolean ok = true;

                for (int i = 0 ; i < TRIES; i++) {
                    field = r.nextInt();
                    if (field != task5()) {
                        ok = false;
                        break;
                    }
                }

                if (ok) {
                    text.setText("OK!- TASK 5");
                } else {
                    text.setText("Wrong.");
                }
            }
        });

        Button b6 = (Button) findViewById(R.id.button6);
        b6.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TextView textV = (TextView) findViewById(R.id.textView6);
                boolean ok = true;

                for (int i = 0 ; i < TRIES; i++) {
                    int j = r.nextInt();
                    task6(j);

                    if (field != j) {
                        ok = false;
                        break;
                    }
                }

                if (ok) {
                    textV.setText("OK!- TASK 6");
                } else {
                    textV.setText("Wrong.");
                }
            }
        });

        Button b7 = (Button) findViewById(R.id.button7);
        b7.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TextView text = (TextView) findViewById(R.id.textView7);
                try {
                    task7();
                    text.setText("Wrong.");
                } catch (Exception e) {
                    text.setText("OK!- TASK 7");
                }
            }
        });

        Button b8 = (Button) findViewById(R.id.button8);
        b8.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TextView text = (TextView) findViewById(R.id.textView8);
                boolean ok = true;

                for (int i = 0 ; i < TRIES; i++) {
                    exception = r.nextBoolean();

                    if (task8() != exception) {
                        ok = false;
                        break;
                    }
                }

                if (ok) {
                    text.setText("OK!- TASK 8");
                } else {
                    text.setText("Wrong.");
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        correct = (TextView) findViewById(R.id.textView4);
    }

    @Override
    protected void onPause() {
        super.onPause();
        correct = null;
    }
}
