#include <jni.h>
#include <string>

extern "C"
jstring
Java_com_example_user_test_MainActivity_stringFromJNI(JNIEnv *env, jobject instance) {

    // TODO

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_user_test_MainActivity_task1(JNIEnv *env, jobject instance) {

    // TODO
    jclass cls = env->FindClass("com/example/user/test/MainActivity");
    jmethodID mID = env->GetMethodID(cls, "setText", "()V");

    env->CallVoidMethod(instance, mID);

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_user_test_MainActivity_task2(JNIEnv *env, jobject instance) {

    // TODO
    jstring str = env->NewStringUTF("OK_TASK2 :)");
    jclass cls = env->FindClass("com/example/user/test/MainActivity");
    jmethodID mID = env->GetMethodID(cls, "setText", "(Ljava/lang/String;)V");

    env->CallVoidMethod(instance, mID, str);
    env->ReleaseStringUTFChars(str, env->GetStringUTFChars(str, NULL));

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_user_test_MainActivity_task3(JNIEnv *env, jobject instance,
                                       jint i) {

    // TODO
//    jstring str = env->NewStringUTF("OK_TASK2 :)");
    jclass cls = env->FindClass("com/example/user/test/MainActivity");
    jmethodID mID = env->GetMethodID(cls, "getInput", "()Ljava/lang/String;");

    jstring str = (jstring) env->CallObjectMethod(instance, mID);

    jint intVal = 2;

    cls = env->FindClass("com/example/user/test/MainActivity");
    mID = env->GetMethodID(cls, "setText", "(Ljava/lang/String;I)V");

    env->CallVoidMethod(instance, mID, str, intVal);
    env->ReleaseStringUTFChars(str, env->GetStringUTFChars(str, NULL));
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_user_test_MainActivity_task4(JNIEnv *env, jobject instance) {

    // TODO

    jclass cls = env->FindClass("com/example/user/test/MainActivity");
    jmethodID mID = env->GetStaticMethodID(cls, "getInt", "()I");

    jint retInt = (jint) env->CallStaticIntMethod(cls, mID);

    //call again the checkInt

    mID = env->GetStaticMethodID(cls, "checkInt", "(I)V");
    env->CallStaticVoidMethod(cls, mID, retInt);

}

extern "C"
JNIEXPORT jint JNICALL
Java_com_example_user_test_MainActivity_task5(JNIEnv *env, jobject instance) {

    // TODO
    jclass cls = env->FindClass("com/example/user/test/MainActivity");
    jfieldID fID = env->GetFieldID(cls, "field", "I");
    jint i = env->GetIntField(instance, fID);

    return i;

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_user_test_MainActivity_task6(JNIEnv *env, jobject instance,
                                       jint i) {

    // TODO
    jclass cls = env->FindClass("com/example/user/test/MainActivity");
    jfieldID fID = env->GetFieldID(cls, "field", "I");
    env->SetIntField(instance, fID, i);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_user_test_MainActivity_task7(JNIEnv *env, jobject instance) {

    // TODO
    jclass exception = env->FindClass("java/lang/Exception");
    env->ThrowNew(exception, "JNI Generated Exception");


}

extern "C"
JNIEXPORT jboolean JNICALL
Java_com_example_user_test_MainActivity_task8(JNIEnv *env, jobject instance) {

    // TODO

    jclass cls = env->FindClass("com/example/user/test/MainActivity");
    jmethodID mID = env->GetMethodID(cls, "generateException", "()V");
    env->CallVoidMethod(instance, mID);

    jboolean ex = env->ExceptionCheck();
    env->ExceptionClear();
    return ex;
}
