package ro.cs.upb.lab7;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final EditText input = (EditText) findViewById(R.id.input_value);

        Button button = (Button) findViewById(R.id.send_button);

        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("HmacSHA256");
            keyGenerator.init(256);
            SecretKey secretKey = keyGenerator.generateKey();
            KeyHolder.setInstance(secretKey);
        } catch (NoSuchAlgorithmException e) {
            Log.e("MainActivity", e.getMessage());
        }



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent();
                i.setAction("ro.cs.upb.lab7.startSecondActivity");
                i.putExtra("msg", input.getText().toString().getBytes());

                try {
                    Mac hmacSha = Mac.getInstance("HmacSHA256");
                    hmacSha.init(KeyHolder.getInstance());
                    byte[] cipherText = Arrays.toString(Hex.encodeHex(hmacSha.doFinal(input.getText().toString().getBytes()))).getBytes();
                    i.putExtra("msgEnc", cipherText);
                } catch (NoSuchAlgorithmException | InvalidKeyException e) {
                    e.printStackTrace();
                }

                //task 4
                try {
                    Cipher cipher = Cipher.getInstance("AES");
                    byte[] iv = new byte[cipher.getBlockSize()];
                    SecureRandom secureRandom = new SecureRandom();
                    secureRandom.nextBytes(iv);
                    IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
                    cipher.init(Cipher.ENCRYPT_MODE, KeyHolder.getInstance(), ivParameterSpec);

                    byte[] cipherText = cipher.doFinal(input.getText().toString().getBytes());

                    i.putExtra("task4Msg", cipherText);
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
                    Log.e("MainActivity", e.getMessage());
                }



                startActivity(i);
            }
        });
    }

}
