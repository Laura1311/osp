package ro.cs.upb.lab7;

import javax.crypto.SecretKey;

/**
 * @version $Id$
 */
public class KeyHolder {

    private static SecretKey instance;
    //private KeyHolder() {}

    public static void setInstance(SecretKey secretKey) {
        instance = secretKey;
    }

    public static SecretKey getInstance() {
        return  instance;
    }
}
