package ro.cs.upb.lab7;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;


public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        TextView output = (TextView) findViewById(R.id.second_text);
        TextView outputTask3 = (TextView) findViewById(R.id.task3_text);
        TextView outputTask4 = (TextView) findViewById(R.id.task4_text);

        Intent intent = getIntent();

        if (intent != null && intent.getByteArrayExtra("msg") != null) {
            byte[] inputMain = intent.getByteArrayExtra("msg");

            if (intent.getByteArrayExtra("msgEnc") != null) {

                Mac hmacSha = null;
                byte[] inputMainEnc = intent.getByteArrayExtra("msgEnc");
                try {
                    hmacSha = Mac.getInstance("HmacSHA256");
                    hmacSha.init(KeyHolder.getInstance());
                    byte[] cipherText = Arrays.toString(Hex.encodeHex(hmacSha.doFinal(inputMain))).getBytes();

                    if (Arrays.equals(cipherText, inputMainEnc)) {
                        outputTask3.setText("TASK3: Data is unmodified!");
                    } else {
                        outputTask3.setText("TASK3: Data modified! :(");
                    }

                } catch (NoSuchAlgorithmException | InvalidKeyException e) {
                    e.printStackTrace();
                }

                //task 4

                try {
                    Cipher cipher = Cipher.getInstance("AES");
                    byte[] iv = new byte[cipher.getBlockSize()];
                    SecureRandom secureRandom = new SecureRandom();
                    secureRandom.nextBytes(iv);
                    IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

                    cipher.init(Cipher.DECRYPT_MODE, KeyHolder.getInstance(), ivParameterSpec);

                    byte[] decryptedData = cipher.doFinal(intent.getByteArrayExtra("task4Msg"));

                    if (Arrays.equals(decryptedData, inputMain)) {
                        outputTask4.setText("TASK4: Data is unmodified!" + "\nDecrypted message is: " + new String(decryptedData));
                    }
                } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
                    e.printStackTrace();
                }
            }
            output.setText("Received msg bytes : " + Arrays.toString(inputMain) + "\n Received msg string: " + new String(inputMain));
        }
    }
}
