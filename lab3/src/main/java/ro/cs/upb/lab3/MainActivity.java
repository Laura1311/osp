package ro.cs.upb.lab3;

import android.app.Activity;
import android.content.*;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {

    TextView textView;
    ServiceConnection serviceConnection;
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            double result = intent.getDoubleExtra(Constants.OUTPUT_PI, 0);
            textView.setText("Output PI: " + String.valueOf(result));
        }
    };

    BoundService boundService;
    boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText enteredValue = (EditText) findViewById(R.id.enter_value);
        Button button = (Button) findViewById(R.id.computeButton);
        textView = (TextView) findViewById(R.id.output_text);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.RECEIVE_ACTION);

        registerReceiver(broadcastReceiver, intentFilter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (enteredValue.getText() == null || enteredValue.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "INVALID INPUT", Toast.LENGTH_LONG).show();
                    return;
                }

                Intent intent = new Intent(MainActivity.this, PiService.class);
                intent.putExtra(Constants.INPUT, Integer.valueOf(enteredValue.getText().toString()));
                startService(intent);

            }
        });


        //TASK 2
        Button showNotification = (Button) findViewById(R.id.show);
        Button hideNotification = (Button) findViewById(R.id.hide);

        showNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Task2Service.class);
                intent.setAction(Constants.START_FOREGROUND);
                startService(intent);
            }
        });
        hideNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Task2Service.class);
                intent.setAction(Constants.STOP_FOREGROUND);
                startService(intent);
            }
        });


        //TASK 3
        final Button boundServiceButton = (Button) findViewById(R.id.task3_button);


        final Handler mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Bundle data = msg.getData();
                // if the data has the "pi_value" key, retrieve it
                // and set it as the text of the TextView element

                double result = data.getDouble(Constants.OUTPUT_PI);
                textView.setText("Output PI: " + String.valueOf(result));
            }
        };


        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                BoundService.MyBinder myBinder = (BoundService.MyBinder) service;
                boundService = myBinder.getService();
                mBound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mBound = false;
            }
        };
        Intent i = new Intent(this, BoundService.class);
        bindService(i, serviceConnection, Context.BIND_AUTO_CREATE);

        boundServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enteredValue.getText() == null || enteredValue.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "INVALID INPUT", Toast.LENGTH_LONG).show();
                    return;
                }
                if (boundService != null) {
                    boundService.computePi(Integer.valueOf(enteredValue.getText().toString()), mHandler);
                }
            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (mBound) {
            unbindService(serviceConnection);
            mBound = false;
        }
    }
}
