package ro.cs.upb.lab3;

import android.app.Service;
import android.content.Intent;
import android.os.*;

/**
 * @version $Id$
 */
public class ExtService extends Service {
    private final MyBinder myBinder = new MyBinder();

    private final IExtService.Stub mBinder = new IExtService.Stub() {

        @Override
        public double computePi(int precision) throws RemoteException {
            PiComputer piComputer = new PiComputer(precision);
            return piComputer.compute();
        }

        public void basicTypes(int anInt, long aLong, boolean aBoolean,
                               float aFloat, double aDouble, String aString) {
            // Does nothing
        }
    };


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public void computePi(int precision, final Handler callback) {
        new PiCompute().execute(precision);
    }

    private class PiCompute extends AsyncTask<Integer, Void, Double> {

        @Override
        protected Double doInBackground(Integer... params) {
            PiComputer piComputer = new PiComputer(params[0]);
            return piComputer.compute();
        }

        @Override
        protected void onPostExecute(Double aDouble) {

        }
    }


    public class MyBinder extends Binder {
        ExtService getService() {
            return ExtService.this;
        }
    }
}
