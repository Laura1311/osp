package ro.cs.upb.lab3;

import android.app.Service;
import android.content.Intent;
import android.os.*;

/**
 * @version $Id$
 */
public class BoundService extends Service {
    private final MyBinder myBinder = new MyBinder();
    private Handler callback;

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int i = intent.getIntExtra(Constants.INPUT, 0);
        new PiCompute().execute(i);
        return START_STICKY;
    }

    public void computePi(int precision, final Handler callback) {
        this.callback = callback;
        new PiCompute().execute(precision);
    }



    private class PiCompute extends AsyncTask<Integer, Void, Double> {

        @Override
        protected Double doInBackground(Integer... params) {
            PiComputer piComputer = new PiComputer(params[0]);
            return piComputer.compute();
        }

        @Override
        protected void onPostExecute(Double aDouble) {
            // after the value of PI has been computed
            Message msg = callback.obtainMessage();
            Bundle args = new Bundle();
            args.putDouble(Constants.OUTPUT_PI, aDouble);
            msg.setData(args);
            msg.sendToTarget();
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public class MyBinder extends Binder {
        BoundService getService() {
            return BoundService.this;
        }
    }
}
