package ro.cs.upb.lab3;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * @version $Id$
 */
public class Task2Service extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent i = new Intent(this, MainActivity.class);
        PendingIntent pending = PendingIntent.getActivity(this, 0, i, 0);
        Notification.Builder notifBuilder = new Notification.Builder(this);
        notifBuilder.setContentIntent(pending);
        notifBuilder.setSmallIcon(R.drawable.app_icon);
        notifBuilder.setContentText("TASK 2 NOTIFICATION");

        Notification notification = notifBuilder.build();

        if (intent.getAction().equals(Constants.START_FOREGROUND)) {
            startForeground(Notification.FLAG_ONGOING_EVENT, notification);
        }

        if (intent.getAction().equals(Constants.STOP_FOREGROUND)) {
            stopForeground(true);
        }
        return START_STICKY;
    }
}
