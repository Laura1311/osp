package ro.cs.upb.lab3;

/**
 * @version $Id$
 */
public class Constants {
    public static final String RECEIVE_ACTION = "RECEIVE_ACTION";
    public static final String INPUT = "INPUT";
    public static final String OUTPUT_PI = "OUTPUT_PI";
    public static final String START_FOREGROUND = "START_FOREGROUND";
    public static final String STOP_FOREGROUND = "STOP_FOREGROUND";
    public static final String PI_COMPUTE = "PI_COMPUTE";
}
