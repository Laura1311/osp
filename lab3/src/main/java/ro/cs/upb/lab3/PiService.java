package ro.cs.upb.lab3;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

/**
 * @version $Id$
 */
public class PiService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int i = intent.getIntExtra(Constants.INPUT, 0);
        new PiCompute().execute(i);

        return START_STICKY;
    }



    private class PiCompute extends AsyncTask<Integer, Void, Double> {

        @Override
        protected Double doInBackground(Integer... params) {
            PiComputer piComputer = new PiComputer(params[0]);
            return piComputer.compute();
        }

        @Override
        protected void onPostExecute(Double aDouble) {
            //broadcast with the result
            Intent intent = new Intent();

            intent.setAction(Constants.RECEIVE_ACTION);
            intent.putExtra(Constants.OUTPUT_PI, aDouble);
            sendBroadcast(intent);
        }
    }

}
