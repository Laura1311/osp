// IExtService.aidl
package ro.cs.upb.lab3;

// Declare any non-default types here with import statements

interface IExtService {

    double computePi(int precision);

    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat,
            double aDouble, String aString);
}