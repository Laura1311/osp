package ro.cs.upb.lab4;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


public class MainActivity extends Activity implements LocationListener {

    private TextView textView;

    private LocationManager locationManager;
    private TextView locationText;

    private BluetoothAlarmReceiver bluetoothReceiver;
    BluetoothAdapter adapter;

    private WifiManager wifiManager;
    private WifiAlarmReceiver wifiAlarmReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final int CODE = 5; // app defined constant used for onRequestPermissionsResult

        String[] permissionsToRequest =
                {
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                };

        boolean allPermissionsGranted = true;

        for(String permission : permissionsToRequest)
        {
            allPermissionsGranted = allPermissionsGranted && (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED);
        }

        if(!allPermissionsGranted)
        {
            ActivityCompat.requestPermissions(this, permissionsToRequest, CODE);
        }

//        mBluetoothAdapter.startDiscovery();

        //Task 1
        final EditText enteredValue = (EditText) findViewById(R.id.url_value);
        Button button = (Button) findViewById(R.id.computeButton);
        textView = (TextView) findViewById(R.id.output_text);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enteredValue.getText() == null || enteredValue.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "INVALID INPUT", Toast.LENGTH_LONG).show();
                    return;
                }

                // to enter value easy example:
                // adb shell input text "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=48.8589507,2.2775174\&radius=500\&type=museum\&key=AIzaSyDm1Nes4hK-BGDOCKtacS11m8gCLZ2qdl0"

                new Task1().execute(enteredValue.getText().toString());
            }
        });


        //Task 2
        Button locationButton = (Button) findViewById(R.id.location_button);
        locationText = (TextView) findViewById(R.id.location_text);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Location location = checkLocation();
                if (location != null) {
                    locationText.setText("Location Changed: \nLat: " + location.getLatitude() + "\nLong: " + location.getLongitude());
                }

            }
        });

        //Task 3
        final LocationAlarmReceiver alarmReceiver = new LocationAlarmReceiver();
        Button startAlarm = (Button) findViewById(R.id.start_alarm);
        Button stopAlarm = (Button) findViewById(R.id.stop_alarm);

        startAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarmReceiver.setAlarm(MainActivity.this);
            }
        });

        stopAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarmReceiver.cancelAlarm(MainActivity.this);
            }
        });

        //Task 4
        adapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothReceiver = new BluetoothAlarmReceiver();
        Button startBluetooth = (Button) findViewById(R.id.start_bluetooth);
        Button stopBluetooth = (Button) findViewById(R.id.stop_bluetooth);

        startBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothReceiver.setAlarm(MainActivity.this);
            }
        });

        stopBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bluetoothReceiver.cancelAlarm(MainActivity.this);
            }
        });

        //Task 5

        wifiAlarmReceiver = new WifiAlarmReceiver();
        wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);

        Button startWifiScan = (Button) findViewById(R.id.start_wifi_scan);
        Button stopWifiScan = (Button) findViewById(R.id.stop_wifi_scan);

        startWifiScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wifiAlarmReceiver.setAlarm(MainActivity.this);
            }
        });

        stopWifiScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wifiAlarmReceiver.cancelAlarm(MainActivity.this);
            }
        });


    }

    private Location checkLocation() {
        Location location = null;
        for (String locationProvider : locationManager.getProviders(true)) {
            locationManager.requestLocationUpdates(locationProvider, 6000, 5, MainActivity.this);
            location = locationManager.getLastKnownLocation(locationProvider);
            if (location != null) {
                break;
            }
        }
        return location;
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(locationManager.getBestProvider(new Criteria(), false), 400, 1, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bluetoothReceiver != null) {
            try {
                unregisterReceiver(bluetoothReceiver);
            } catch (IllegalArgumentException e) {
                Log.e("MainActivity", "SILENT ERROR: " + e.getMessage());
            }
        }

        if (wifiAlarmReceiver != null) {
            try {
                unregisterReceiver(wifiAlarmReceiver);
            } catch (IllegalArgumentException e) {
                Log.e("MainActivity", "SILENT ERROR: " + e.getMessage());
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        locationText.setText("Location Changed: \nLat: " + location.getLatitude() + "\nLong: " + location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled provider " + provider, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider, Toast.LENGTH_LONG).show();
    }

    //Task 1

    private class Task1 extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            URL url = null;
            InputStream is = null;
            try {
                url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder stringBuilder = new StringBuilder();

                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line).append("\n");
                }
                return stringBuilder.toString();

            } catch (MalformedURLException e) {
                Log.d("MainActivity", "Cannot connect!");
            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            textView.setText(s);
            super.onPostExecute(s);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    //Task 3

    public class LocationAlarmReceiver extends BroadcastReceiver {

        private static final String TAG = "LocationAlarmReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO
            // Get the last know location and log it
            // There is no need to verify the intent since it will be triggered only through the setAlarm method
            Location location = checkLocation();
            Toast.makeText(MainActivity.this, "ALARM\n Lat: " + location.getLatitude() + "\nLong: " + location.getLongitude(), Toast.LENGTH_LONG).show();
            Log.d(TAG, "Lat: " + location.getLatitude() + "\nLong: " + location.getLongitude());
        }

        public void setAlarm(Context context) {
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent i = new Intent(context, LocationAlarmReceiver.class);
            PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
            am.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 2, pi);
        }

        public void cancelAlarm(Context context) {
            Intent intent = new Intent(context, LocationAlarmReceiver.class);
            PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(sender);
        }
    }

    //Task 4

    public class BluetoothAlarmReceiver extends BroadcastReceiver {
        private static final String TAG = "BluetoothAlarmReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_DISCOVERY_STARTED)) {

                Log.v(TAG, "Starting Bluetooth discovery");
                adapter.startDiscovery();
            }

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Toast.makeText(MainActivity.this, "NEW BT DEVICE: " + device.getName(), Toast.LENGTH_LONG).show();
            }
        }

        public void setAlarm(Context context) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
            intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            adapter.enable();
            //start scan when alarm is set
            adapter.startDiscovery();
            registerReceiver(bluetoothReceiver, intentFilter);

            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent i = new Intent(context, BluetoothAlarmReceiver.class);
            PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
            am.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 3, pi);
        }

        public void cancelAlarm(Context context) {
            Intent intent = new Intent(context, BluetoothAlarmReceiver.class);
            if (bluetoothReceiver != null) {
                try {
                    unregisterReceiver(bluetoothReceiver);
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "SILENT ERROR: " + e.getMessage());
                }
            }
            PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(sender);
        }
    }

    //Task 5

    public class WifiAlarmReceiver extends BroadcastReceiver {
        private static final String TAG = "WifiAlarmReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            List<ScanResult> scanResults = wifiManager.getScanResults();

            for (ScanResult s : scanResults) {
                Toast.makeText(MainActivity.this, "NEW WIFI DEVICE: " + s.BSSID, Toast.LENGTH_LONG).show();
            }
        }

        public void setAlarm(Context context) {
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent i = new Intent(context, WifiAlarmReceiver.class);
            registerReceiver(this, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
            am.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 3, pi);
        }

        public void cancelAlarm(Context context) {
            Intent intent = new Intent(context, WifiAlarmReceiver.class);
            PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
            if (wifiAlarmReceiver != null) {
                try {
                    unregisterReceiver(wifiAlarmReceiver);
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "SILENT ERROR: " + e.getMessage());
                }
            }
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(sender);
        }
    }

}
